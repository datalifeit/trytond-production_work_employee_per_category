# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['TimesheetWork', 'Timesheet']


class TimesheetWork(metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    center_category = fields.Many2One('production.work.center.category',
        'Work center category', ondelete='RESTRICT')


class Timesheet(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    def get_work_center(self, name=None):
        if self.employee:
            params = {}
            if self.work.center_category:
                params['center_category'] = self.work.center_category.id
            return self.employee.compute_work_center(self.date, **params)
        return None
