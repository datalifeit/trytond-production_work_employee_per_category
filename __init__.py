# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .employee import Employee, EmployeeWorkCenter
from .timesheet import Timesheet, TimesheetWork
from .labor_yield import LaborYieldAllocation, LaborYieldAllocate


def register():
    Pool.register(
        Timesheet,
        TimesheetWork,
        module='production_work_employee_per_category', type_='model',
        depends=['timesheet'])
    Pool.register(
        LaborYieldAllocation,
        module='production_work_employee_per_category', type_='model',
        depends=['labor_yield'])
    Pool.register(
        Employee,
        EmployeeWorkCenter,
        module='production_work_employee_per_category', type_='model')
    Pool.register(
        LaborYieldAllocate,
        module='production_work_employee_per_category', type_='wizard',
        depends=['labor_yield'])
