datalife_production_work_employee_per_category
==============================================

The production_work_employee_per_category module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-production_work_employee_per_category/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-production_work_employee_per_category)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
